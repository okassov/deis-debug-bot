# coding: utf-8

import sys
import os
import time
import random
import string
import requests
import json
import re
import stat
import hashlib
import subprocess
from os.path import dirname
import zbxtg_settings


class TelegramAPI:
    tg_url_bot_general = "https://api.telegram.org/bot"

    def http_get(self, url):
        answer = requests.get(url, proxies=self.proxies)
        self.result = answer.json()
        self.ok_update()
        return self.result

    def __init__(self, key):
        self.debug = False
        self.key = key
        self.proxies = {}
        self.type = "private"  # 'private' for private chats or 'group' for group chats
        self.disable_notification = False
        self.tmp_dir = None
        self.tmp_uids = None
        self.update_offset = 0
        self.image_buttons = False
        self.result = None
        self.ok = None
        self.error = None
        self.get_updates_from_file = False

    def get_me(self):
        url = self.tg_url_bot_general + self.key + "/getMe"
        me = self.http_get(url)
        return me

    def get_updates(self):
        url = self.tg_url_bot_general + self.key + "/getUpdates"
        params = {"offset": self.update_offset}
        answer = requests.post(url, params=params, proxies=self.proxies)
        self.result = answer.json()
        if self.get_updates_from_file:
            print_message("Getting updated from file getUpdates.txt")
            self.result = json.loads("".join(file_read("getUpdates.txt")))
        self.ok_update()
        return self.result

    def send_message(self, to, message):
        url = self.tg_url_bot_general + self.key + "/sendMessage"
        message = "\n".join(message)
        params = {"chat_id": to, "text": message,
                  "disable_notification": self.disable_notification}
        answer = requests.post(url, params=params, proxies=self.proxies)
        if answer.status_code == 414:
            self.result = {"ok": False, "description": "414 URI Too Long"}
        else:
            self.result = answer.json()
        self.ok_update()
        return self.result

    def update_message(self, to, message_id, message):
        url = self.tg_url_bot_general + self.key + "/editMessageText"
        message = "\n".join(message)
        params = {"chat_id": to, "message_id": message_id, "text": message,
                  "disable_notification": self.disable_notification}
        answer = requests.post(url, params=params, proxies=self.proxies)
        self.result = answer.json()
        self.ok_update()
        return self.result

    def send_photo(self, to, path):
        url = self.tg_url_bot_general + self.key + "/sendPhoto"
        if self.image_buttons:
            reply_markup = json.dumps({"inline_keyboard": [[
                {"text": "R", "callback_data": "graph_refresh"},
                {"text": "1h", "callback_data": "graph_period_3600"},
                {"text": "3h", "callback_data": "graph_period_10800"},
                {"text": "6h", "callback_data": "graph_period_21600"},
                {"text": "12h", "callback_data": "graph_period_43200"},
                {"text": "24h", "callback_data": "graph_period_86400"},
            ], ]})
        else:
            reply_markup = json.dumps({})
        params = {"chat_id": to, "disable_notification": self.disable_notification,
                  "reply_markup": reply_markup}
        files = {"photo": open(path, 'rb')}
        answer = requests.post(url, params=params, files=files, proxies=self.proxies)
        self.result = answer.json()
        self.ok_update()
        return self.result

    def send_txt(self, to, text, text_name=None):
        path = self.tmp_dir + "/" + "zbxtg_txt_"
        url = self.tg_url_bot_general + self.key + "/sendDocument"
        text = "\n".join(text)
        if not text_name:
            path += "".join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))
        else:
            path += text_name
        path += ".txt"
        file_write(path, text)
        params = {"chat_id": to, "caption": path.split("/")[-1], "disable_notification": self.disable_notification}
        answer = requests.post(url, params=params, files=files, proxies=self.proxies)
        self.result = answer.json()
        self.ok_update()
        return self.result

    def get_uid(self, name):
        uid = 0
        updates = self.get_updates()
        for m in updates["result"]:
            if "message" in m:
                chat = m["message"]["chat"]
            elif "edited_message" in m:
                chat = m["edited_message"]["chat"]
            else:
                continue
            if chat["type"] == self.type == "private":
                if "username" in chat:
                    if chat["username"] == name:
                        uid = chat["id"]
            if (chat["type"] == "group" or chat["type"] == "supergroup") and self.type == "group":
                if "title" in chat:
                    if sys.version_info[0] < 3:
                        if chat["title"] == name.decode("utf-8"):
                            uid = chat["id"]
                    else:
                        if chat["title"] == name:
                            uid = chat["id"]
        return uid

    def error_need_to_contact(self, to):
        if self.type == "private":
            print_message("User '{0}' needs to send some text bot in private".format(to))
        if self.type == "group":
            print_message("You need start a conversation with your bot first in '{0}' group chat, type '/start@{1}'"
                          .format(to, self.get_me()["result"]["username"]))

    def update_cache_uid(self, name, uid, message="Add new string to cache file"):
        cache_string = "{0};{1};{2}\n".format(name, self.type, str(uid).rstrip())
        # FIXME
        with open(self.tmp_uids, "a") as cache_file_uids:
            cache_file_uids.write(cache_string)
        return True

    def get_uid_from_cache(self, name):
        uid = 0
        if os.path.isfile(self.tmp_uids):
            with open(self.tmp_uids, 'r') as cache_file_uids:
                cache_uids_old = cache_file_uids.readlines()
            for u in cache_uids_old:
                u_splitted = u.split(";")
                if name == u_splitted[0] and self.type == u_splitted[1]:
                    uid = u_splitted[2]
        return uid

    def answer_callback_query(self, callback_query_id, text=None):
        url = self.tg_url_bot_general + self.key + "/answerCallbackQuery"
        if not text:
            params = {"callback_query_id": callback_query_id}
        else:
            params = {"callback_query_id": callback_query_id, "text": text}
        answer = requests.post(url, params=params, proxies=self.proxies)
        self.result = answer.json()
        self.ok_update()
        return self.result

    def ok_update(self):
        self.ok = self.result["ok"]
        if self.ok:
            self.error = None
        else:
            self.error = self.result["description"]
            print_message(self.error)
        return True





class ZabbixWeb:
    def __init__(self, server, username, password):
        self.debug = True
        self.server = server
        self.username = username
        self.password = password
        self.proxies = {}
        self.verify = True
        self.cookie = None
        self.basic_auth_user = None
        self.basic_auth_pass = None
        self.tmp_dir = None

    def login(self):
        if not self.verify:
            requests.packages.urllib3.disable_warnings()

        data_api = {"name": self.username, "password": self.password, "enter": "Sign in"}
        answer = requests.post(self.server + "/", data=data_api, proxies=self.proxies, verify=self.verify,
                               auth=requests.auth.HTTPBasicAuth(self.basic_auth_user, self.basic_auth_pass))
        cookie = answer.cookies
        if len(answer.history) > 1 and answer.history[0].status_code == 302:
            print_message("probably the server in your config file has not full URL (for example "
                          "'{0}' instead of '{1}')".format(self.server, self.server + "/zabbix"))
        if not cookie:
            print_message("authorization has failed, url: {0}".format(self.server + "/"))
            cookie = None

        self.cookie = cookie

    def graph_get(self, graph_id, period, width, height, version=3):
        file_img = self.tmp_dir + "/{0}.png".format("".join(graph_id))

        colors = {
            0: "00CC00",
            1: "CC0000",
            2: "0000CC",
            3: "CCCC00",
            4: "00CCCC",
            5: "CC00CC",
        }

        drawtype = 5
        if len(graph_id) > 1:
            drawtype = 2

        zbx_img_url = self.server + "chart2.php?graphid={0}&from={1}+00%3A00%3A00&to=now&" \
        "profileIdx=web.graphs.filter&profileIdx2={0}" \
        "&width={2}&height={3}".format(graph_id, period, '900', '200')

        answer = requests.get(zbx_img_url, cookies=self.cookie, proxies=self.proxies, verify=self.verify,
                              auth=requests.auth.HTTPBasicAuth(self.basic_auth_user, self.basic_auth_pass))
        status_code = answer.status_code
        if status_code == 404:
            print_message("can't get image from '{0}'".format(zbx_img_url))
            return False
        res_img = answer.content
        file_bwrite(file_img, res_img)
        return file_img

    def api_test(self):
        headers = {'Content-type': 'application/json'}
        api_data = json.dumps({"jsonrpc": "2.0", "method": "user.login", "params":
                              {"user": self.username, "password": self.password}, "id": 1})
        api_url = self.server + "/api_jsonrpc.php"
        api = requests.post(api_url, data=api_data, proxies=self.proxies, headers=headers)
        return api.text


def file_write(filename, text):
    with open(filename, "w") as fd:
        fd.write(str(text))
    return True

def file_bwrite(filename, data):
    with open(filename, "wb") as fd:
        fd.write(data)
    return True

def file_read(filename):
    with open(filename, "r") as fd:
        text = fd.readlines()
    return text

def file_append(filename, text):
    with open(filename, "a") as fd:
        fd.write(str(text))
    return True

def main():
    tmp_dir = zbxtg_settings.zbx_tg_tmp_dir

    tmp_cookie = tmp_dir + "/cookie.py.txt"
    tmp_uids = tmp_dir + "/uids.txt"
    tmp_need_update = False  # do we need to update cache file with uids or not

    rnd = random.randint(0, 999)
    ts = time.time()
    hash_ts = str(ts) + "." + str(rnd)
    
    args = sys.argv

    settings = {
        "zbxtg_image_width": "900",
        "zbxtg_image_height": "200",
        "tg_method_image": True,  # if True - default send images, False - send text
        "tg_chat": False,  # send message to chat or in private
        "tg_group": False,  # send message to chat or in private
        "is_channel": False,
        "is_single_message": False,
        "to": None,
        "to_group": False,
        "forked": False,
    }

    zbx_to = args[1]
    zbx_graph_id = args[2]
    zbx_start_date = args[3]

    tg = TelegramAPI(key=zbxtg_settings.tg_key)

    tg.tmp_dir = tmp_dir
    tg.tmp_uids = tmp_uids

    if zbxtg_settings.proxy_to_tg:
        proxy_to_tg = zbxtg_settings.proxy_to_tg
        if not proxy_to_tg.find("http") and not proxy_to_tg.find("socks"):
            proxy_to_tg = "https://" + proxy_to_tg
        tg.proxies = {
            "https": "{0}".format(proxy_to_tg),
        }

    zbx = ZabbixWeb(server=zbxtg_settings.zbx_server, 
                    username=zbxtg_settings.zbx_api_user,
                    password=zbxtg_settings.zbx_api_pass)

    zbx.tmp_dir = tmp_dir

    zbx_version = 4

    try:
        zbx_version = zbxtg_settings.zbx_server_version
    except:
        pass

    if zbxtg_settings.proxy_to_zbx:
        zbx.proxies = {
            "http": "http://{0}/".format(zbxtg_settings.proxy_to_zbx),
            "https": "https://{0}/".format(zbxtg_settings.proxy_to_zbx)
        }

    try:
        zbx_api_verify = zbxtg_settings.zbx_api_verify
        zbx.verify = zbx_api_verify
    except:
        pass


    tg_method_image = bool(settings["tg_method_image"])
    tg_chat = bool(settings["tg_chat"])
    tg_group = bool(settings["tg_group"])
    is_channel = bool(settings["is_channel"])
    is_single_message = bool(settings["is_single_message"])

    # experimental way to send message to the group
    if args[0].split("/")[-1] == "zbxtg_group.py" or "--group" in args or tg_chat or tg_group:
        tg_chat = True
        tg_group = True
        tg.type = "group"

    if not os.path.isdir(tmp_dir):
        try:
            os.makedirs(tmp_dir)
            open(tg.tmp_uids, "a").close()
            os.chmod(tmp_dir, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
            os.chmod(tg.tmp_uids, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
        except:
            tmp_dir = "/tmp"


    done_all_work_in_the_fork = False
    # issue75

    to_types = ["to", "to_group", "to_channel"]
    to_types_to_telegram = {"to": "private", "to_group": "group", "to_channel": "channel"}
    multiple_to = {}
    for i in to_types:
        multiple_to[i]=[]

    for t in to_types:
        try:
            if settings[t] and not settings["forked"]:
                # zbx_to = settings["to"]
                multiple_to[t] = re.split(",", settings[t])
        except KeyError:
            pass

    # example:
    # {'to_channel': [], 'to': ['usr1', 'usr2', 'usr3'], 'to_group': []}

    if (sum([len(v) for k, v in list(multiple_to.items())])) == 1:
        # if we have only one recipient, we don't need fork to send message, just re-write "to" vaiable
        tmp_max = 0
        for t in to_types:
            if len(multiple_to[t]) > tmp_max:
                tmp_max = len(multiple_to[t])
                tg.type = to_types_to_telegram[t]
                zbx_to = multiple_to[t][0]
    else:
        for t in to_types:
            for i in multiple_to[t]:
                args_new = list(args)
                args_new[1] = i
                if t == "to_group":
                    args_new.append("--group")
                args_new.append("--forked")
                args_new.insert(0, sys.executable)
                subprocess.call(args_new)
                done_all_work_in_the_fork = True

    if done_all_work_in_the_fork:
        sys.exit(0)

    uid = None

    if tg.type == "channel":
        uid = zbx_to
    if tg.type == "private":
        zbx_to = zbx_to.replace("@", "")

    if zbx_to.isdigit():
        uid = zbx_to

    if not uid:
        uid = tg.get_uid_from_cache(zbx_to)

    if not uid:
        uid = tg.get_uid(zbx_to)
        if uid:
            tmp_need_update = True
    if not uid:
        tg.error_need_to_contact(zbx_to)
        sys.exit(1)

    if tmp_need_update:
        tg.update_cache_uid(zbx_to, str(uid).rstrip())


    message_id = 0
    if tg_method_image:
        zbx.login()
        if not zbx.cookie:
            text_warn = "Login to Zabbix web UI has failed (web url, user or password are incorrect), "\
                        "unable to send graphs check manually"
            tg.send_message(uid, [text_warn])
            print_message(text_warn)
        else:
            zbxtg_file_img = zbx.graph_get(zbx_graph_id, zbx_start_date, settings["zbxtg_image_width"], 
                                               settings["zbxtg_image_height"], 
                                               version=zbx_version)
            if tg.ok:
                message_id = tg.result["result"]["message_id"]
            tg.reply_to_message_id = message_id
            if not zbxtg_file_img:
                text_warn = "Can't get graph image, check script manually, see logs, or disable graphs"
                tg.send_message(uid, [text_warn])
                print_message(text_warn)
            else:
                if not is_single_message:
                    zbxtg_body_text = ""
                else:
                    if is_modified:
                        text_warn = "probably you will see MEDIA_CAPTION_TOO_LONG error, "\
                                    "the message has been cut to 200 symbols, "\
                                    "https://github.com/ableev/Zabbix-in-Telegram/issues/9"\
                                    "#issuecomment-166895044"
                        print_message(text_warn)
                if not is_single_message:
                    tg.disable_notification = True
                tg.send_photo(uid, zbxtg_file_img)
                print (zbxtg_file_img)
                if tg.ok:
                    settings["zbxtg_body_text"] = zbxtg_body_text
                    os.remove(zbxtg_file_img)
                else:
                    if tg.error.find("PHOTO_INVALID_DIMENSIONS") > -1:
                        text_warn = "Zabbix user couldn't get graph (probably has no rights to get data from host), " \
                                    "check script manually, see {0}".format(url_wiki_base + "/" +
                                                                            settings_description["graphs"]["url"])
                        tg.send_message(uid, [text_warn])
                        print_message(text_warn)


if __name__ == "__main__":
    main()